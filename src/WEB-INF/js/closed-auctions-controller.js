/**
 * de.sb.broker.WelcomeController: broker welcome controller.
 * Copyright (c) 2013-2015 Sascha Baumeister
 */
"use strict";

this.de = this.de || {};
this.de.sb = this.de.sb || {};
this.de.sb.broker = this.de.sb.broker || {};
(function () {
	var SUPER = de.sb.broker.Controller;

	/**
	 * Creates a new welcome controller that is derived from an abstract controller.
	 * @param sessionContext {de.sb.broker.SessionContext} a session context
	 */
	de.sb.broker.ClosedAuctionsController = function (sessionContext) {
		SUPER.call(this, 2, sessionContext);
	}
	de.sb.broker.ClosedAuctionsController.prototype = Object.create(SUPER.prototype);
	de.sb.broker.ClosedAuctionsController.prototype.constructor = de.sb.broker.ClosedAuctionsController;


	/**
	 * Displays the associated view.
	 */
	de.sb.broker.ClosedAuctionsController.prototype.display = function () {
		this.sessionContext.clear();
		SUPER.prototype.display.call(this);
		
		var sectionElement = document.querySelector("#closed-auctions-template").content.cloneNode(true).firstElementChild;

		de.sb.util.AJAX.invoke("/services/auctions", "GET", { "Accept" : "application/json" }, null, null, function() {
		
			alert('foo');
		});
		
		document.querySelector("main").appendChild(sectionElement);
	}
} ());
